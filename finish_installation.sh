#/bin/bash

mkdir referenced-mods
cd referenced-mods/
git clone https://github.com/minetest/minetest_game
git clone https://github.com/minetest-mods/unified_inventory
git clone https://github.com/bdjnk/mini_sun
git clone https://github.com/Ezhh/under_sky
git clone https://notabug.org/TenPlus1/ethereal
git clone https://github.com/orwell96/advtrains
git clone https://github.com/Jeija/minetest-mod-mesecons
git clone https://github.com/BlockMen/cme
git clone https://github.com/duane-r/underworlds
git clone https://github.com/saavedra29/archer
git clone https://github.com/stujones11/minetest-3d_armor
mv minetest-3d_armor/ 3d_armor/

mkdir octacian
cd octacian
git clone https://github.com/octacian/digicompute
git clone https://github.com/octacian/datalib
git clone https://github.com/octacian/microexpansion

cd ..
mkdir yawin
cd yawin
git clone https://gitlab.com/yawin/Mod_Magic
git clone https://github.com/yawin123/utilities

cd ../../mods/
rm -f 3d_armor advtrains archer beds boats bucket carts cme creative datalib default digicompute doors dye ethereal farming fire flowers give_initial_stuff microexpansion mini_sun Mod_Magic minetest-mod-mesecons Minetest-WorldEdit player_api screwdriver sethome sfinv stairs tnt under_sky underworlds unified_inventory utilities vessels walls wool xpanes

ln -s ../referenced-mods/minetest_game/mods/beds/
ln -s ../referenced-mods/minetest_game/mods/boats/
ln -s ../referenced-mods/minetest_game/mods/bucket/
ln -s ../referenced-mods/minetest_game/mods/carts/
ln -s ../referenced-mods/minetest_game/mods/creative/
ln -s ../referenced-mods/minetest_game/mods/default/
ln -s ../referenced-mods/minetest_game/mods/doors/
ln -s ../referenced-mods/minetest_game/mods/dye/
ln -s ../referenced-mods/minetest_game/mods/farming/
ln -s ../referenced-mods/minetest_game/mods/fire/
ln -s ../referenced-mods/minetest_game/mods/flowers/
ln -s ../referenced-mods/minetest_game/mods/give_initial_stuff/
ln -s ../referenced-mods/minetest_game/mods/player_api/
ln -s ../referenced-mods/minetest_game/mods/screwdriver/
ln -s ../referenced-mods/minetest_game/mods/sethome/
ln -s ../referenced-mods/minetest_game/mods/sfinv/
ln -s ../referenced-mods/minetest_game/mods/stairs/
ln -s ../referenced-mods/minetest_game/mods/tnt/
ln -s ../referenced-mods/minetest_game/mods/vessels/
ln -s ../referenced-mods/minetest_game/mods/walls/
ln -s ../referenced-mods/minetest_game/mods/wool/
ln -s ../referenced-mods/minetest_game/mods/xpanes/

ln -s ../referenced-mods/mini_sun/
ln -s ../referenced-mods/3d_armor/
ln -s ../referenced-mods/cme/
ln -s ../referenced-mods/underworlds/
ln -s ../referenced-mods/archer/

ln -s ../referenced-mods/advtrains/
ln -s ../referenced-mods/ethereal/
ln -s ../referenced-mods/minetest-mod-mesecons/
ln -s ../referenced-mods/under_sky/
ln -s ../referenced-mods/unified_inventory/

ln -s ../referenced-mods/octacian/digicompute/
ln -s ../referenced-mods/octacian/datalib/
ln -s ../referenced-mods/octacian/microexpansion/

ln -s ../referenced-mods/yawin/Mod_Magic/ magic
ln -s ../referenced-mods/yawin/utilities/
